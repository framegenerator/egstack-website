var express = require('express');    //Express Web Server 
var path = require('path');     //used for file path
var bodyParser = require('body-parser')

var app = express();
app.use(express.static(path.join(__dirname, 'public')));
app.use('/website_routes/uploads', express.static(__dirname + '/website_routes/uploads'));

// parse application/x-www-form-urlencoded
// app.use(bodyParser.urlencoded({ extended: false }))
 
// parse application/json
app.use(bodyParser.json())

var server = app.listen(3030, function() {
    console.log('Listening on port %d', server.address().port);
});

///////////////////////////////////
// PYTHON
///////////////////////////////////

//=========================================================================
// Mnist route
require('./website_routes/route_MNIST.js')(app, bodyParser);
//=========================================================================

//=========================================================================
// Caption route
require('./website_routes/route_caption.js')(app);
//=========================================================================





